#include <Wire.h>
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <IRremote.h>

#include "RTClib.h"

#include"Constante.h"
#include"Senzori.h"
#include"CClock.h"

IRrecv irrecv(Pini::pin_IR);
decode_results results;
bool ledVerdePornit = 0;
bool ledRosuPornit = 1;
LiquidCrystal lcd(0);

enum modDisplay { modCeas, modSenzori, modSetari } modCurent;

void setup() {
	CClock::init();
	Serial.begin(9600);

	pinMode(Pini::pin_ledVerde, OUTPUT);
	pinMode(Pini::pin_ledRosu, OUTPUT);
	irrecv.enableIRIn();

	lcd.begin(16, 2);
	lcd.setBacklight(HIGH);

	digitalWrite(Pini::pin_ledRosu, HIGH);

	modCurent = modCeas;
}


void afisareCeas() {
	lcd.setCursor(0, 0);
	lcd.print(CClock::getCurrentTime());	
	
	lcd.setCursor(0, 1);
	lcd.print(CClock::getCurrentDate());
}
void afisareSetari() {
	lcd.setCursor(0, 0);
	lcd.print("Mod setari:");
}
void afisareSenzori() {
	lcd.setCursor(0, 0);
	lcd.print("Temp: ");
	lcd.print(Senzori::citesteTempCelsius());


	lcd.setCursor(0, 1);
	lcd.print("Lum:  ");
	lcd.print(Senzori::citesteLux());
}

void loop() {
	if (Serial.available()) {
		char op = (char)Serial.read();
		char msg[30];	
		char *date;
		char *time;
		int i = 0;
		float x;

		delay(50); 

		switch (op)
		{
		case '1': //Seteaza data si timpul
			while (Serial.available())
				msg[i++] = Serial.read();
			msg[i] = '\0';
			i = 0;
			
			date = strtok(msg, ";");
			time = strtok(NULL, ";");

			CClock::setDateTime(date,time);
			break;
		case '2': //Returneaza data si timpul
			Serial.println(CClock::getCurrentDateTimeInUnixTime());
			break;
		case '3': //Returneaza starea senzorilor
			

			Serial.println(Senzori::citesteTempCelsius());
			Serial.println(Senzori::citesteLux());
	
			break;
		default:
			break;
		}

		while (Serial.available()) {
			char t = Serial.read();
		}
	}

	if (irrecv.decode(&results)){
		if (modCurent != modSetari) {
			switch (results.value) {
			case Butoane::butonON:
				digitalWrite(Pini::pin_ledVerde, ledVerdePornit ? LOW : HIGH);
				ledVerdePornit ^= 1;
				break;
			case Butoane::butonMUTE:
				lcd.setBacklight(ledRosuPornit ? LOW : HIGH);
				digitalWrite(Pini::pin_ledRosu, ledRosuPornit ? LOW : HIGH);
				ledRosuPornit ^= 1;
				break;
			case Butoane::butonDreapta:
				lcd.clear();
				modCurent = modDisplay::modCeas;
				break;
			case Butoane::butonStanga:
				lcd.clear();
				modCurent = modDisplay::modSenzori;
				break;
			case Butoane::butonOK:
				lcd.clear();
				modCurent = modDisplay::modSetari;
				break;
			}
		}
		else {
			switch (results.value){
			case Butoane::butonOK:
				//Salvare setari
				modCurent = modDisplay::modCeas;
				
				lcd.clear();

				break;
			}
		}
		irrecv.resume();
	}

	if (modCurent == modDisplay::modCeas)
		afisareCeas();
	else if (modCurent == modDisplay::modSenzori)
		afisareSenzori();
	else if (modCurent == modDisplay::modSetari)
		afisareSetari();
}





