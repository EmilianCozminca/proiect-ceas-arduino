import serial
import os
import time
import base64
import datetime

ser = serial.Serial('COM3', 9600,timeout = 10)
clear = lambda: os.system('cls')

time.sleep(2)

def setHour():
    ser.write('1'.encode('ascii'))
    time.sleep(0.03)

    ser.write(time.strftime("%b %d %Y;%X").encode('ascii'))	
def readHour():
    ser.write('2'.encode('ascii'))
    a = int(str(ser.readline(), 'ascii'))
    print(datetime.datetime.fromtimestamp(a).strftime('%Y-%m-%d %H:%M:%S'))

def readSensors():
    ser.write('3'.encode('ascii'))
    time.sleep(0.01)
    a = str(ser.readline(),'ascii')
    b = str(ser.readline(),'ascii')

    print(a)
    print(b)

while 1:

	print("Script gestionare Ceas-Arduino \n")
	print("1. Actualizeaza ora")
	print("2. Citeste ora")
	print("3. Citeste senzori")

	optiune = { '1' : setHour,
				'2' : readHour,
				'3' : readSensors,	
	}
	
	c = input()
	clear()
	optiune[c]()