#include"Senzori.h"
#include"Constante.h"
#include<Arduino.h>
namespace Senzori{
  float citesteTempCelsius(){
      float voltage = analogRead(Pini::pin_TMP) * 5.0;
      voltage /= 1024.0; 
      return ((voltage - 0.5) * 100);
   }
   char* citesteLux(){
	   short intensitate = analogRead(Pini::pin_Lumina);
	   if (intensitate < 10)
		   return "L1";
	   else if (intensitate < 200)
		   return "L2";
	   else if (intensitate < 500)
		   return "L3";
	   else if (intensitate < 800)
		   return "L4";
	   else
		   return "L5";
   }
}
