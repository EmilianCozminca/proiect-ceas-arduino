#pragma once
namespace Pini{
	const int pin_TMP = 2;
	const int pin_Lumina = 3; 
	const int pin_IR = 9;
	const int pin_ledRosu = 10;
	const int pin_ledVerde = 11;
}
namespace Butoane{
  const long butonON		= 0x58853BCE;
  const long butonMUTE		= 0x58853BEA;
  const long buton1			= 0x58853BE0;
  const long buton2			= 0x58853BD0;
  const long buton3			= 0x58853BF0;
  const long buton4			= 0x58853BC8;
  const long buton5			= 0x58853BE8;
  const long buton6			= 0x58853BD8;
  const long buton7			= 0x58853BF8;
  const long buton8			= 0x58853BC4;
  const long buton9			= 0x58853BE4;
  const long buton0			= 0x58853BC0;
  const long butonStanga	= 0x58853BDE;
  const long butonDreapta	= 0x58853BFE;
  const long butonSus		= 0x58853BF6;
  const long butonJos		= 0x58853BD6;
  const long butonOK		= 0x58853BC2;
}
