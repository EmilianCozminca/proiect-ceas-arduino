#include "RTClib.h"
#include <Wire.h>
#include"CClock.h"
#include<Arduino.h>
#include <Wire.h>

namespace CClock{
	RTC_DS1307 RTC;

  long getCurrentDateTimeInUnixTime() {
	  return RTC.now().unixtime();
  }
  void setDateTime(char date[], char time[])
  {
	  RTC.adjust(DateTime(date, time));
  }

  String getCurrentTime() {
	  DateTime now = RTC.now();
	  char tbs[10];
	  sprintf(tbs, "%02d/%02d/%4d", now.day(), now.month(), now.year());
	  return tbs;
  }
  String getCurrentDate() {
	  DateTime now = RTC.now();
	  char tbs[8];
	  sprintf(tbs, "%02d:%02d:%02d", now.hour(), now.minute(), now.second());
	  return tbs;
  }
  void init() {
	  Wire.begin();
	  RTC.begin();
  }
}

