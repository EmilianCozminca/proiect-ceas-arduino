#pragma once

namespace CClock{
  String getCurrentTime();
  String getCurrentDate();
  long getCurrentDateTimeInUnixTime();

  void setDateTime(char[] , char[]);

  void init();
}