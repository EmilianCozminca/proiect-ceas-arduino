A simple Arduino project I did for fun.

Components used:

* Arduino UNO for the microcontroller
* DS1307 for the clock
* I2C display ( https://www.robofun.ro/lcd_20x4_i2c_negru_verde )
* IR sensor for remote control
* Temperature sensor
* Light sensor
* 5V voltage regulator